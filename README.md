# GitPod template for NodeJS modules

NodeJS + Jest Gitpod starter for NPM modules projects.


[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/ziggornif/nodejs-jest-starter/-/tree/main/)

## Content

**NodeJS current version : 16.x**

**NPM current version : 8.x**

- Hello world sample module
- Jest configuration
- Junit reporter
- Eslint + prettier configuration
- Gitlab CI pipeline configuration

## Configuration

### Create new project from this template

- Fork the project or import it as new project from the [GitLab external import page](https://gitlab.com/projects/new#cicd_for_external_repo).

- Update the `Open in Gitpod` README link.

- Update the package.json name and description attributes.

### Setup CI/CD variables 

- Create a personal access token with API scope

- In the CI/CD Settings Variables section, create a variable named `GITLAB_TOKEN` with the previous generated token

That's it 🎉