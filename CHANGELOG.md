# 1.0.0 (2022-06-11)


### Features

* init template ([1abdeda](https://gitlab.com/ziggornif/nodejs-jest-starter/commit/1abdeda9e7f3c355a7b23e62bbd2f7e9b605727e))
* **release:** add semantic release job ([c5127d7](https://gitlab.com/ziggornif/nodejs-jest-starter/commit/c5127d7c333200123920a642aba8a08ddcc05528))
