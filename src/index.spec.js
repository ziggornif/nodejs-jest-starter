const helloPackage = require('./index');

describe('Hello package', () => {
  it('should say hello world', () => {
    const resp = helloPackage();
    expect(resp).toBe('Hello world 👋');
  });

  it('should say hello to somebody', () => {
    const resp = helloPackage('Jean-Michel');
    expect(resp).toBe('Hello Jean-Michel 👋');
  });

  it('should say hello world when input name parameter is null', () => {
    const resp = helloPackage();
    expect(resp).toBe('Hello world 👋');
  });
});
