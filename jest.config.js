/**
 * @type {import('@jest/types').Config.GlobalConfig}
 */
 module.exports = {
  roots: ['<rootDir>', '<rootDir>/src'],
  testEnvironment: 'node',
  reporters: ['default', 'jest-junit'],
  coveragePathIgnorePatterns: ['/node_modules/', '/coverage/'],
  coverageDirectory: '<rootDir>/coverage/',
  testPathIgnorePatterns: ['node_modules'],
  collectCoverage: true,  
};